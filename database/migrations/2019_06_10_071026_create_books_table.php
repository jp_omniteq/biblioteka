<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->unsignedBigInteger('author_id');
            $table->unsignedBigInteger('publishing_house_id');
            $table->unsignedBigInteger('category_id');
            $table->float('price');
            $table->date('purchase_date');
            $table->string('cover');
            $table->boolean('is_read');
            $table->string('description');
            $table->string('my_review');
            $table->integer('my_rate');
            $table->string('ISBN');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
