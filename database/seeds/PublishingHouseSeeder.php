<?php

use Illuminate\Database\Seeder;

class PublishingHouseSeeder extends Seeder
{
    public function run()
    {
        DB::table('publishing_houses')->insert([
            [
                'name' => 'ZNAK',
                'KRS' => '0000064794'
            ],
            [
                'name' => 'Virtualo',
                'KRS' => '0000349437'
            ],
            [
                'name' => 'Simon & Schuster',
                'KRS' => '0000276897'
            ],
            [
                'name' => 'Wydawnictwo Literackie',
                'KRS' => '0000012638'
            ]
        ]);
    }
}