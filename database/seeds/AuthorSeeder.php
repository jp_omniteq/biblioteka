<?php

use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    public function run()
    {
        DB::table('authors')->insert([
            [
                'name' => 'Adam',
                'surname' => 'Mickiewicz',
                'year_of_birth' => '1798',
                'description' => 'Wieszcz narodowy'
            ],
            [
                'name' => 'Aleksander',
                'surname' => 'Fredro',
                'year_of_birth' => '1793',
                'description' => 'Hrabia'
            ],
            [
                'name' => 'Walter',
                'surname' => 'Isaacson',
                'year_of_birth' => '1952',
                'description' => 'amerykański pisarz i biograf'
            ],
            [
                'name' => 'Yuval Noah',
                'surname' => 'Harari',
                'year_of_birth' => '1976',
                'description' => 'izraelski historyk i profesor na Wydziale Historii Uniwersytetu Hebrajskiego w Jerozolimie'
            ]
        ]);
    }
}