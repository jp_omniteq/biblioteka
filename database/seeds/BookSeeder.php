<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    public function run()
    {
        DB::table('books')->insert([
            [
                'name' => 'Pan Tadeusz',
                'author_id' => 1,
                'publishing_house_id' => 1,
                'category_id' => 1,
                'price' => 19.99,
                'purchase_date' => Carbon::parse('2015-01-03'),
                'cover' => 'www.link.de/image.jpg',
                'is_read' => true,
                'description' => 'Historia pana Tadka i jego perypetie',
                'my_review' => 'super ksiazka, moja ulubiona',
                'my_rate' => 5,
                'ISBN' => '9788387024017'
            ],
            [
                'name' => 'Dziady cz.2 ',
                'author_id' => 1,
                'publishing_house_id' => 1,
                'category_id' => 2,
                'price' => 29.99,
                'purchase_date' => Carbon::parse('2012-02-04'),
                'cover' => 'www.bebe.pl/image.jpg',
                'is_read' => true,
                'description' => 'Dziady i duchy',
                'my_review' => 'No taka srednia, troche nudzi',
                'my_rate' => 3,
                'ISBN' => '9788379035496'
            ],
            [
                'name' => 'Pan Tadeusz 13 ksiega ',
                'author_id' => 2,
                'publishing_house_id' => 2,
                'category_id' => 3,
                'price' => 3.99,
                'purchase_date' => Carbon::parse('2018-02-04'),
                'cover' => 'www.wow.pl/image.jpg',
                'is_read' => true,
                'description' => 'Kontynuacja Pana Tadeusz',
                'my_review' => 'Ciekawe, ale to nie ksiazka dla dzieci, jakies zbereźne rzeczy',
                'my_rate' => 5,
                'ISBN' => '9696969696969'
            ],
            [
                'name' => 'Steve Jobs',
                'author_id' => 3,
                'publishing_house_id' => 3,
                'category_id' => 4,
                'price' => 39.50,
                'purchase_date' => Carbon::parse('2019-03-05'),
                'cover' => 'www.google.pl/image.jpg',
                'is_read' => true,
                'description' => 'Biografia Stevea Jobsa',
                'my_review' => 'Fajna biografia wizjonera',
                'my_rate' => 4,
                'ISBN' => '9788365315205'
            ],
            [
                'name' => 'Sapiens. Od zwierząt do bogów',
                'author_id' => 4,
                'publishing_house_id' => 4,
                'category_id' => 5,
                'price' => 49.99,
                'purchase_date' => Carbon::parse('2019-04-06'),
                'cover' => 'www.google.pl/image.png',
                'is_read' => false,
                'description' => 'Światowy hit. Całościowa historia człowieka – od ukrywania się w jaskiniach do podróży na Księżyc i inżynierii genetycznej',
                'my_review' => 'Nie wiem, nie czytalem',
                'my_rate' => 3,
                'ISBN' => '9788365315205'
            ]
        ]);
    }
}