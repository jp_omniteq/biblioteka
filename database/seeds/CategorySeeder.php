<?php


use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'epopeja'],
            ['name' => 'dramat'],
            ['name' => 'erotyk'],
            ['name' => 'biografia'],
            ['name' => 'Popularnonaukowa']
        ]);
    }
}