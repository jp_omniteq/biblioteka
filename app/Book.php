<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Book extends Model
{
    use Sortable;

    protected $fillable = [
        'name',
        'author_id',
        'publishing_house_id',
        'category_id',
        'price',
        'purchase_date',
        'cover',
        'is_read',
        'description',
        'my_review',
        'my_rate',
        'ISBN'
    ];

    public $sortable = [
    'name',
    'author_id',
    'publishing_house_id',
    'category_id',
    'price',
    'purchase_date',
    'cover',
    'is_read',
    'description',
    'my_review',
    'my_rate',
    'ISBN'
    ];
}
