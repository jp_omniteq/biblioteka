<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Author extends Model
{
    Use Sortable;


    protected $fillable = [
        'name',
        'surname',
        'year_of_birth',
        'description'
    ];

    public $sortable = [
        'name',
        'surname',
        'year_of_birth',
        'description'
    ];
}
