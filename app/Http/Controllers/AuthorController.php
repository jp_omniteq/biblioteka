<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::sortable()->paginate(20);
        return view('layouts/authors',compact('authors'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms/authorNew');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $author = new Author;
        $author -> name = $request->get('name');
        $author -> surname = $request->get('surname');
        $author -> year_of_birth = $request->get('year_of_birth');
        $author -> description = $request->get('description');
        $author ->save();
        return redirect('authors')->with('success', 'Autor został dodany');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Author::find($id);
        return view('forms/author', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        $author = Author::find($id);
        $author -> name = $request->get('name');
        $author -> surname = $request->get('surname');
        $author -> year_of_birth = $request->get('year_of_birth');
        $author -> description = $request->get('description');
        $author ->save();
        return redirect('authors')->with('success', 'Autor został zaktualizowany');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Author::destroy($id);
        return redirect('authors')->with('success', 'Autor został usunięty');
    }
}
