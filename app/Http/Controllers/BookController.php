<?php

namespace App\Http\Controllers;

use App\Book;
use App\Author;
use Illuminate\Http\Request;
use App\Category;
use App\PublishingHouse;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::sortable()->paginate(20);
        return view('layouts/books',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms/bookNew');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $book = new Book;
        $book->cover = 'null';
          $book->name = $request->get('name');
          $book->author_id = $request->get('author_id');
          $book->publishing_house_id = $request->get('publishing_house_id');
          $book->category_id = $request->get('category_id');
          $book->price = $request->get('price');
          $book->purchase_date = $request->get('purchase_date');
          $book->is_read = $request->get('is_read');
          $book->description = $request->get('description');
          $book->my_review = $request->get('my_review');
          $book->my_rate = $request->get('my_rate');
          $book->ISBN = $request->get('ISBN');
          $book->save();
    
          return redirect('books')->with('success', 'Ksiazka została dodana');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author_id = Book::find($id,['author_id']);
        $category_id = Book::find($id,['category_id']);
        $publishinghouse_id = Book::find($id,['publishing_house_id']);

        $category_name = Category::find($category_id,['name']);
        $categories = Category::all(['id','name']);

        $publishinghouse_name = PublishingHouse::find($publishinghouse_id,['name']);
        $publishinghouses = PublishingHouse::all(['id','name']);

        $author_name = Author::find($author_id,['name', 'surname']);
        $authors = Author::all(['id','name','surname']);

        $book = Book::find($id);

        $selected_author_id = $book->author_id;
        $selected_category_id = $book->category_id;
        $selected_publishinghouse_id = $book->publishing_house_id;

        return view('forms/book', compact('book','authors','author_name','categories','publishinghouses', 'selected_author_id','selected_category_id','selected_publishinghouse_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'share_name'=>'required',
        //     'share_price'=> 'required|integer',
        //     'share_qty' => 'required|integer'
        //   ]);
    
        
          $book = Book::find($id);
          $book->name = $request->get('name');
          $book->author_id = $request->get('author_id');
          $book->publishing_house_id = $request->get('publishing_house_id');
          $book->category_id = $request->get('category_id');
          $book->price = $request->get('price');
          $book->purchase_date = $request->get('purchase_date');
          $book->is_read = $request->get('is_read');
          $book->description = $request->get('description');
          $book->my_review = $request->get('my_review');
          $book->my_rate = $request->get('my_rate');
          $book->ISBN = $request->get('ISBN');
          $book->save();
    
          return redirect('books')->with('success', 'Ksiazka została zaktualizowana');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::destroy($id);
        return redirect('books')->with('success', 'Ksiazka została usunięta');
    }
}
