<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $books = Book::sortable()->paginate(20);
        return view('home', compact('books'));

        //return view('home');
    }

    public function search()
    {
        $searchParam = Input::get('searchParam');
        if ($searchParam != "") {
            $book = Book::select('books.*')
                ->where('books.name', 'LIKE', '%' . $searchParam . '%')
                ->join('authors', 'books.author_id', '=', 'authors.id')
                ->orWhere('books.my_rate', 'LIKE', '%' . $searchParam . '%')
                ->orWhere('authors.name', 'LIKE', '%' . $searchParam . '%')
                ->orWhere('authors.surname', 'LIKE', '%' . $searchParam . '%')
                ->get();
            if (count($book) > 0)
                return view('layouts/searchedBooks')->withDetails($book)->withQuery($searchParam);
        }
        return view('layouts/searchedBooks')->withMessage("Nie znaleziono książek!");
    }
}
