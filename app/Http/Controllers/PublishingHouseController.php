<?php

namespace App\Http\Controllers;

use App\PublishingHouse;
use Illuminate\Http\Request;

class PublishingHouseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publishingHouses = PublishingHouse::sortable()->paginate(20);
        return view('layouts/publishinghouses',compact('publishingHouses'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms/publishinghouseNew');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $publishingHouse = new PublishingHouse;
        $publishingHouse -> name = $request->get('name');
        $publishingHouse -> KRS = $request->get('KRS');
        $publishingHouse -> save();
        return redirect('publishing-houses')->with('success', 'Wydawnictwo zostało dodane');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PublishingHouse  $publishingHouse
     * @return \Illuminate\Http\Response
     */
    public function show(PublishingHouse $publishingHouse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PublishingHouse  $publishingHouse
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publishinghouse = PublishingHouse::find($id);
        return view('forms/publishinghouse', compact('publishinghouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PublishingHouse  $publishingHouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $publishingHouse = PublishingHouse::find($id);
        $publishingHouse -> name = $request->get('name');
        $publishingHouse -> KRS = $request->get('KRS');
        $publishingHouse -> save();
        return redirect('publishing-houses')->with('success', 'Wydawnictwo zostało zaktualizowane');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PublishingHouse  $publishingHouse
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PublishingHouse::destroy($id);
        return redirect('publishing-houses')->with('success', 'Wydawnictwo zostało usuniete');
    }
}
