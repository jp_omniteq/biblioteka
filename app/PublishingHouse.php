<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PublishingHouse extends Model
{
    use Sortable;
    protected $fillable = [
        'name',
        'KRS'
    ];

    public $sortable = [
        'name',
        'KRS'
        
    ];
}
