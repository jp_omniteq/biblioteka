<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// Show index

Route::get('/', 'HomeController@index')->name('home');
Route::get('/books', 'BookController@index')->name('books');
Route::get('/authors', 'AuthorController@index')->name('authors');
Route::get('/publishing-houses', 'PublishingHouseController@index')->name('publishinghouses');
Route::get('/categories', 'CategoryController@index')->name('categories');

// Edit
Route::get('/books/{id}/edit', 'BookController@edit')->name('editbook');
Route::get('/categories/{id}/edit', 'CategoryController@edit')->name('editcategory');
Route::get('/publishinghouses/{id}/edit', 'PublishingHouseController@edit')->name('editpublishinghouse');
Route::get('/authors/{id}/edit', 'AuthorController@edit')->name('editauthor');

Route::patch('/book/{id}', 'BookController@update')->name('bookupdate');
Route::patch('/category/{id}', 'CategoryController@update')->name('categoryupdate');
Route::patch('/publishinghouse/{id}', 'PublishingHouseController@update')->name('publishinghouseupdate');
Route::patch('/author/{id}', 'AuthorController@update')->name('authorupdate');

// Search

Route::post('/search', 'HomeController@search')-> name('search');

// New
Route::get('/book/create', 'BookController@create')->name('bookcreate');
Route::post('/book/store', 'BookController@store')->name('bookstore');

Route::get('/author/create', 'AuthorController@create')->name('authorcreate');
Route::post('/author/store', 'AuthorController@store')->name('authorstore');

Route::get('/publishinghouse/create', 'PublishingHouseController@create')->name('publishinghousecreate');
Route::post('/publishinghouse/store', 'PublishingHouseController@store')->name('publishinghousestore');

Route::get('/category/create', 'CategoryController@create')->name('categorycreate');
Route::post('/category/store', 'CategoryController@store')->name('categorystore');

// Delete
Route::delete('books/{id}/delete','BookController@destroy')->name('deletebook');
Route::delete('authors/{id}/delete','AuthorController@destroy')->name('deleteauthor');
Route::delete('categories/{id}/delete','CategoryController@destroy')->name('deletecategory');
Route::delete('publishinghouse/{id}/delete','PublishingHouseController@destroy')->name('deletepublishinghouse');