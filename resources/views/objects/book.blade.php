<div class="container">
    @if(isset($details))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>@sortablelink('name', 'Nazwa')</th>
                <th>@sortablelink('description', 'Opis')</th>
                <th>@sortablelink('price', 'Cena')</th>
                <th>@sortablelink('is_read', 'Przeczytana')</th>
                <th>@sortablelink('my_rate', 'Moja ocena')</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($details as $book)
                <tr>
                    <td>{{ $book->name }}</td>
                    <td>{{ $book->description }}</td>
                    <td>{{ $book->price }}</td>
                    <td><input type="checkbox" disabled {{  $book->is_read ? 'checked' : '' }} ></td>
                    <td>{{ $book->my_rate }}</td>
                    <td style="width: 165px" >

                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    {{-- <a href=""> is always GET so we need a form to perform DELETE  --}}
                                    <form action="{{ route('deletebook', $book->id ) }}" method="post">
                                        <button class="btn btn-danger cursor-pointer" type="submit" value="Delete">
                                            <span class="fa fa-trash-o "></span>
                                        </button>
                                        @method('delete')
                                        @csrf
                                    </form>
                                </div>
                                <div class="col-sm">
                                    <a role="button" href="{{ route('editbook',$book->id ) }}" class="btn btn-primary cursor-pointer">
                                        <span class="fa fa-pencil-square-o"></span>
                                    </a>
                                </div>


                            </div>
                        </div>





                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>