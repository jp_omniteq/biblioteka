@extends('layouts/app')

@section('title')
Twoja Biblioteka
@endsection

@section('content')

<div class="container py-4"><h1>Twoje ulubione ksiąki</h1></div>


@isset($books)
@include('lists/books', $books)
@endisset


@endsection

