@extends('layouts/app')
@section('content')

    <div class="container">
        <div class="container py-4">
          
                <h1>Dodaj nowe wydawnictwo</h1>
            
        </div>

        <div class="container">
            <div class="card uper">
                <div class="card-header">
                    Dodaj wydawnictwo
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form method="post" action="{{ route('publishinghousestore') }}">
                        @method('POST')
                        @csrf
                        <div class="form-group">
                            <label for="name">Nazwa:</label>
                            <input type="text" class="form-control" name="name"  />
                        </div>
                        <div class="form-group">
                            <label for="name">KRS:</label>
                            <input type="text" class="form-control" name="KRS"  />
                        </div>
                        <a href="{{ URL::previous() }}" class="btn btn-danger mr-5">Anuluj</a>
                        <button type="submit" class="btn btn-primary">Zapisz</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
