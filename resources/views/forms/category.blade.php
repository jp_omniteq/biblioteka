@extends('layouts/app')
@section('content')

<div class="container">
    <div class="container py-4">
        @isset($category)
        <h1>{{$category->name}}</h1>
        @else
        <h1>Dodaj nową kategorię</h1>
        @endisset
    </div>

    <div class="container">
            <div class="card uper">
              <div class="card-header">
                Edytuj kategorię
              </div>
              <div class="card-body">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                  <form method="post" action="{{ route('categoryupdate', $category->id) }}">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                      <label for="name">Nazwa:</label>
                      <input type="text" class="form-control" name="name" value={{ $category->name }} />
                    </div>
                    <a href="{{ URL::previous() }}" class="btn btn-danger mr-5">Anuluj</a>
                    <button type="submit" class="btn btn-primary">Zapisz</button>
                
                  </form>
              </div>
            </div>
    </div>
</div>

@endsection
