@extends('layouts/app')
@section('content')

<div class="container">
    <div class="container py-4">
       
        <h1>Dodaj nową książkę</h1>
      
    </div>

    <div class="container">
            <div class="card uper">
                <div class="card-header">
                    Dodaj ksiąkę
                </div>
              <div class="card-body">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                  <form method="post" action="{{ route('bookstore') }}">
                    @method('POST')
                    @csrf
                    <div class="form-group">
                      <label for="name">Nazwa:</label>
                      <input type="text" class="form-control" name="name"  />
                    </div>
                    <div class="form-group">
                      <label for="price">Autor:</label>
                      <input type="text" class="form-control" name="author_id"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Przeczytana:</label>
                      <input type="text" class="form-control" name="is_read"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Kategoria:</label>
                      <input type="text" class="form-control" name="category_id"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Cena:</label>
                      <input type="text" class="form-control" name="price"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Wydawnictwo:</label>
                      <input type="text" class="form-control" name="publishing_house_id"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Data zakupu:</label>
                      <input type="text" class="form-control" name="purchase_date" />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Opis:</label>
                      <input type="text" class="form-control" name="description"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Recenzja:</label>
                      <input type="text" class="form-control" name="my_review"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">Ocena:</label>
                      <input type="text" class="form-control" name="my_rate"  />
                    </div>
                    <div class="form-group">
                      <label for="quantity">ISBN:</label>
                      <input type="text" class="form-control" name="ISBN"  />
                    </div>
                    <a href="{{ URL::previous() }}" class="btn btn-danger mr-5">Anuluj</a>
                    <button type="submit" class="btn btn-primary">Zapisz</button>
                
                  </form>
              </div>
            </div>
          



    </div>
</div>

@endsection
