@extends('layouts/app')
@section('content')

    <div class="container">
        <div class="container py-4">
            @isset($author)
                <h1>{{$author->name}}</h1>
            @else
                <h1>Dodaj nowego autora</h1>
            @endisset
        </div>

        <div class="container">
            <div class="card uper">
                <div class="card-header">
                    Edytuj Autora
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form method="post" action="{{ route('authorupdate', $author->id) }}">
                        @method('PATCH')
                        @csrf
                        <div class="form-group">
                            <label for="name">Imię:</label>
                            <input type="text" class="form-control" name="name" value={{ $author->name }} />
                        </div>
                        <div class="form-group">
                            <label for="name">Nazwisko:</label>
                            <input type="text" class="form-control" name="surname" value={{ $author->surname }} />
                        </div>
                        <div class="form-group">
                            <label for="name">Rok urodzenia:</label>
                            <input type="text" class="form-control" name="year_of_birth" value={{ $author->year_of_birth }} />
                        </div>
                        <div class="form-group">
                            <label for="name">Opis:</label>
                            <input type="text" class="form-control" name="description" value={{ $author->description }} />
                        </div>

                        <a href="{{ URL::previous() }}" class="btn btn-danger mr-5">Anuluj</a>
                        <button type="submit" class="btn btn-primary">Zapisz</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
