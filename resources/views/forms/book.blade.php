@extends('layouts/app')
@section('content')

<div class="container">
    <div class="container py-4">
      
        <h1>{{$book->name}}</h1>
     
    </div>

    <div class="container">
            <div class="card uper">
              <div class="card-header">
                Edytuj książkę
              </div>
              <div class="card-body">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                  <form method="post" action="{{ route('bookupdate', $book->id) }}">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                      <label for="name">Nazwa:</label>
                      <input type="text" class="form-control" name="name" value="{{ $book->name }}" />
                    </div>
                    <div class="form-group">
                    
                      @isset ($authors)
                      <label for="author_id">Autor:</label>
                      <select class="form-control" id="author_id" name="author_id">
                        @foreach ($authors as $author)
                      <option value={{ $author->id }}  {{ ( $author->id == $selected_author_id) ? 'selected' : '' }}> {{ $author->name.' '.$author->surname }} </option>
                        @endforeach
                      </select>
                      @endif
                    </div>
                    <div class="form-check mb-3">
                     
                      <input type="checkbox" class="form-check-input" name="is_read" value="1" @if ( $book->is_read)
                          checked
                      @endif />
                      <label class="form-check-label" for="is_read">Przeczytana</label>
                    </div>
                    <div class="form-group">
                      

                      @isset ($categories)
                      <label for="category_id">Kategoria:</label>
                      <select class="form-control" id="category_id" name="category_id">
                        @foreach ($categories as $category)
                      <option value={{ $category->id }}  {{ ( $category->id == $selected_category_id) ? 'selected' : '' }}> {{ $category->name }} </option>
                        @endforeach
                      </select>
                      @endif

                    </div>
                    <div class="form-group">
                      <label for="price">Cena:</label>
                      <input type="text" class="form-control" name="price" value={{ $book->price }} />
                    </div>
                    <div class="form-group">
                      
                      @isset ($publishinghouses)
                      <label for="publishing_house_id">Wydawnictwo:</label>
                      <select class="form-control" id="publishing_house_id" name="publishing_house_id">
                        @foreach ($publishinghouses as $publishinghous)
                      <option value={{ $publishinghous->id }}  {{ ( $publishinghous->id == $selected_publishinghouse_id) ? 'selected' : '' }}> {{ $publishinghous->name }} </option>
                        @endforeach
                      </select>
                      @endif


                    </div>
                    <div class="form-group">
                      <label for="purchase_date">Data zakupu:</label>
                      <input type="text" class="form-control" name="purchase_date" value={{ $book->purchase_date }} />
                    </div>
                    <div class="form-group">
                      <label for="description">Opis:</label>
                      <textarea class="form-control" name="description" id="description" cols="30" rows="10">{{$book->description}}</textarea>
                    </div>
                    <div class="form-group">
                      <label for="my_review">Recenzja:</label>
                      <textarea class="form-control" name="my_review" id="my_review" cols="30" rows="10">{{ $book->my_review }}</textarea>
                   
                    </div>
                    <div class="form-group">
                      <label for="my_rate">Ocena:</label>
                      <input type="text" class="form-control" name="my_rate" value={{ $book->my_rate }} />
                    </div>
                    <div class="form-group">
                      <label for="ISBN">ISBN:</label>
                      <input type="text" class="form-control" name="ISBN" value="{{ $book->ISBN }}" />
                    </div>
                    <a href="{{ URL::previous() }}" class="btn btn-danger mr-5">Anuluj</a>
                    <button type="submit" class="btn btn-primary">Zapisz</button>
                
                  </form>
              </div>
            </div>
          



    </div>
</div>

@endsection
