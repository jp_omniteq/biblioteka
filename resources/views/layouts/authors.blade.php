@extends('layouts/app')
@section('content')

<div class="container">
    <div class="container py-4"><h1>Autorzy książek</h1></div>
            

        @include('lists/authors', $authors)
</div>

@endsection
