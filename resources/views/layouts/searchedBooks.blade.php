@extends('layouts/app')
@section('content')

    <div class="container">
        @if(isset($details))
            <div class="container py-4"><h1>Znalezione książki</h1></div>
            @include('objects/book', $details)
        @else
            <div class="container py-4"><h1>{{$message}}</h1></div>
        @endif

    </div>

@endsection