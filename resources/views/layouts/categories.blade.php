@extends('layouts/app')
@section('content')

    <div class="container">
        <div class="container py-4"><h1>Kategorie</h1></div>


        @include('lists/categories', $categories)
    </div>

@endsection
