@extends('layouts/app')
@section('content')

<div class="container">
    <div class="container py-4"><h1>Wydawnictwa</h1></div>
            

        @include('lists/publishinghouses', $publishingHouses)
</div>

@endsection
