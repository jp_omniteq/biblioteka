<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand " href="{{ route('home') }}">
            <img src="/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="Logo książka">
        </a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is('book*') ? 'active' : ''}} cursor-pointer">
                <a class="nav-link" href="{{ route('books') }}">Książki <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{ Request::is('authors') ? 'active': ''}} cursor-pointer">
                <a class="nav-link" href="{{ route('authors') }}">Autorzy</a>
            </li>
            <li class="nav-item {{ Request::is('publishing-houses') ? 'active': ''}} cursor-pointer">
                <a class="nav-link" href="{{ route('publishinghouses') }}">Wydawnictwa</a>
            </li>
            <li class="nav-item {{ Request::is('categories') ? 'active': ''}} cursor-pointer">
                <a class="nav-link" href="{{ route('categories') }}">Kategorie</a>
            </li>

        </ul>


        <!-- Right Side Of Navbar -->
        <form class="pull-right form-inline my-2 my-lg-0 mr-4" action="{{URL::to('/search')}}" method="POST" role="search">
            {{ csrf_field() }}
            <input type="text" class="form-control mr-sm-2" placeholder="Szukaj" aria-label="Search" name="searchParam">
            <button type="submit" class="btn btn-outline-success my-2 my-sm-0">Szukaj</button>
        </form>
        <ul class="navbar-nav">
          
            <!-- Authentication Links -->
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                        {{ __('auth.Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>

</nav>






      