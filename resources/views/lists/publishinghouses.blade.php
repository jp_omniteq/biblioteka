   
@isset($publishingHouses)
    

<div class="container">
      <a role="button" class="btn btn-primary cursor-pointer mb-4" href="{{ route('publishinghousecreate') }}">Dodaj</a>
        <table class="table table-striped">
           <thead>
           <tr>
              <th>@sortablelink('name', 'Nazwa')</th>
              <th>@sortablelink('KRS', 'KRS')</th>
              <th></th>
           </tr>
           </thead>
           <tbody>
              @foreach($publishingHouses as $publishinghouse)
              <tr>
                 <td>{{ $publishinghouse->name }}</td>
                 <td>{{ $publishinghouse->KRS }}</td>
                  <td class="pull-right">

                        <div class="container">
                              <div class="row">
                                    <div class="col-sm">
                                          <form action="{{ route('deletepublishinghouse', $publishinghouse->id ) }}" method="post">
                                                <button class="btn btn-danger cursor-pointer" type="submit" value="Delete">
                                                        <span class="fa fa-trash-o "></span>
                                            </button>
                                                @method('delete')
                                                @csrf
                                            </form>
                                      </div>
                                <div class="col-sm">
                                         
                  <a role="button" href="{{ route('editpublishinghouse',$publishinghouse->id ) }}"
                        class=" btn btn-primary cursor-pointer">
                         <span class="fa fa-pencil-square-o"></span>
                     </a>
                                </div>
                               
                               
                              </div>
                            </div>
                
                 
                  </td>
              </tr>
              @endforeach
           </tbody>
        </table>
        
        {!! $publishingHouses->appends(\Request::except('page'))->render() !!}
     </div>
     @endisset