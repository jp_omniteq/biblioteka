@isset($categories)


    <div class="container">
            <a role="button" class="btn btn-primary cursor-pointer mb-4" href="{{ route('categorycreate') }}">Dodaj</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>@sortablelink('name', 'Nazwa')</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td class="pull-right">
                        
                            <div class="container">
                                    <div class="row">
                                            <div class="col-sm">
                                                    <form action="{{ route('deletecategory', $category->id ) }}" method="post">
                                                            <button class="btn btn-danger cursor-pointer" type="submit" value="Delete">
                                                                    <span class="fa fa-trash-o "></span>
                                                        </button>
                                                            @method('delete')
                                                            @csrf
                                                        </form>
                                              </div>
                                      <div class="col-sm">
                                            <a role="button" href="{{ route('editcategory',$category->id ) }}"
                                                    class="btn btn-primary cursor-pointer">
                                                    <span class="fa fa-pencil-square-o"></span>
                                                </a>
                                      </div>
                                      
                                      
                                    </div>
                                  </div>








{{-- 
                        <a role="button" href="{{ route('editcategory',$category->id ) }}"
                                class="btn btn-primary cursor-pointer">
                                <span class="fa fa-pencil-square-o"></span>
                            </a>
                            <form action="{{ route('deletecategory', $category->id ) }}" method="post">
                                    <button class="mr-4 btn btn-danger cursor-pointer" type="submit" value="Delete">
                                            <span class="fa fa-trash-o "></span>
                                </button>
                                    @method('delete')
                                    @csrf
                                </form> --}}
                        </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $categories->appends(\Request::except('page'))->render() !!}
    </div>
@endisset