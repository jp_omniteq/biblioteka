   
@isset($authors)
    

<div class="container">
      <a role="button" class="btn btn-primary cursor-pointer mb-4" href="{{ route('authorcreate') }}">Dodaj</a>
        <table class="table table-striped">
           <thead>
           <tr>
              <th>@sortablelink('name', 'Nazwa')</th>
              <th>@sortablelink('surname', 'Opis')</th>
              <th>@sortablelink('year_of_birth', 'Cena')</th>
              <th>@sortablelink('description', 'Przeczytana')</th>
              <th></th>
           </tr>
           </thead>
           <tbody>
              @foreach($authors as $author)
              <tr>
                 <td>{{ $author->name }}</td>
                 <td>{{ $author->surname }}</td>
                 <td>{{ $author->year_of_birth }}</td>
                 <td>{{ $author->description }}</td>
                 <td class="pull-right">


                     <div class="container">
                        <div class="row">
                              <div class="col-sm">
                                    <form action="{{ route('deleteauthor', $author->id ) }}" method="post">
                                          <button class="btn btn-danger cursor-pointer" type="submit" value="Delete">
                                                  <span class="fa fa-trash-o "></span>
                                      </button>
                                          @method('delete')
                                          @csrf
                                      </form>
                                </div>
                             <div class="col-sm">
                                 <a role="button" href="{{ route('editauthor',$author->id ) }}"
                                       class=" btn btn-primary cursor-pointer">
                                        <span class="fa fa-pencil-square-o"></span>
                                    </a>
                            
                           </div>
                     </div>















                  
                    
                     
                  </td>
                 
              </tr>
              @endforeach
           </tbody>
        </table>
        
        {!! $authors->appends(\Request::except('page'))->render() !!}
     </div>
     @endisset