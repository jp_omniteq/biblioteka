<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Dane logowania nie pasują do żadnego użytkownika.',
    'throttle' => 'Za dużo prób logowania. Spróbuj ponownie za :seconds sekund.',
    'login' => 'logowanie',
    'Login' => 'Logowanie',
    'E-Mail Address' => 'Adres E-mail',
    'password' => 'hasło',
    'Password' => 'Hasło',
    'Remember Me' => 'Zapamiętaj mnie',
    'Forgot Your Password?' => 'Zapomniałeś hasła?',
    'Logout' => 'Wyloguj'

];
